package Junit_rpn_calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculadoraDividirTest.class, CalculadoraMultiplicarTest.class, CalculadoraSomarTest.class,
		CalculadoraSubtrairTest.class })
public class AllTests {

}
