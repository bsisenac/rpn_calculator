package Junit_rpn_calculator;


import static org.junit.Assert.assertTrue;

import org.junit.Test;
import rpn_calculator.*;

public class CalculadoraMultiplicarTest {
	
	@Test
	public void test() {
		
		Calculadora calc = new Console();
		double result = calc.multiplicar(10, 10);
		assertTrue( 100.0 == result );
		
	}

}
