package Junit_rpn_calculator;


import static org.junit.Assert.assertTrue;

import org.junit.Test;
import rpn_calculator.*;

import rpn_calculator.RPNWarningException;

public class CalculadoraDividirTest {
	
	@Test
	public void test() {
		
		Calculadora calc = new Console();
		double result = calc.dividir( 10, 2 );
		assertTrue( 5.0 == result );
		
	}
}
