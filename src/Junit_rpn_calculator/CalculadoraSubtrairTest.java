package Junit_rpn_calculator;


import static org.junit.Assert.assertTrue;

import org.junit.Test;
import rpn_calculator.*;

public class CalculadoraSubtrairTest {
	
	@Test
	public void test() {
		
		Calculadora calc = new Console();
		double result = calc.subtrair(10, 10);
		assertTrue( 0.0 == result );
		
	}

}
