package Junit_rpn_calculator;


import static org.junit.Assert.assertTrue;

import org.junit.Test;
import rpn_calculator.*;

public class CalculadoraSomarTest {
	
	@Test
	public void test() {
		
		Calculadora calc = new Console();
		double result = calc.somar(10, 10);
		assertTrue( 20.0 == result );
		
	}

}
