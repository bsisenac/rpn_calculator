package rpn_calculator;

public class RPNWarningException extends Exception{
	
	public RPNWarningException( String string ) {
		super( string );
	}
	
}
