package rpn_calculator;

public interface Calculadora {
	
	public double multiplicar( double num1, double num2 );
	public double dividir( double num1, double num2 );
	public double somar( double num1, double num2 );
	public double subtrair( double num1, double num2 );
	
}
