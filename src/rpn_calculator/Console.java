package rpn_calculator;

import java.util.Scanner;
import java.util.Stack;

public class Console implements Calculadora{

	Stack<Double> pilha;

	public Console() {
		this.pilha = new Stack<Double>();
	}

	public Console(Stack<Double> pilha) {
		this.pilha = pilha;
	}

	public void entrarDado(double valor) throws RPNWarningException {

		if (this.pilha.size() == 4)
			throw new RPNWarningException("Limite máximo da fila atingido: 4");
		
		this.pilha.push(valor);

	};

	public void deletarDado() {

		if (!this.pilha.empty())
			this.pilha.removeElementAt(this.pilha.size() - 1);
		
	};

	public void mostrarResultado(){
		
		System.out.println( this.pilha );
		
	};

	public void escolherOperacao( int valor ) throws RPNWarningException {
		
		if( this.pilha.isEmpty() || this.pilha.size() <= 1 )
			throw new RPNWarningException( "Antes de escolher a operação, informe pelo menos dois números." );
		
		switch ( valor ) {
			case 1:
				this.pilha.push( this.somar( this.pilha.pop(), this.pilha.pop() ) );
				break;
		
			case 2:
				this.pilha.push( this.subtrair( this.pilha.pop(), this.pilha.pop() ) );
				break;
			
			case 3:
				this.pilha.push( this.multiplicar( this.pilha.pop(), this.pilha.pop() ) );
				break;
				
			case 4:
				Double arg2 = this.pilha.pop();
	        	
	        	if( arg2 == 0 ){
	        		this.pilha.push( arg2 );
	        		throw new RPNWarningException( "Não é permitido dividir por zero" );
	        	}
	        	
	        	this.pilha.push( this.dividir( this.pilha.pop(), arg2 ) );
				break;
		default:
			throw new RPNWarningException( "Operação selecionada não permitida" );
			
		}
		
	};

	public static void main(String[] args) throws RPNWarningException {

		Console con = new Console();
		Scanner in = new Scanner(System.in);	
		System.out.println("Bem-vindo a Calculadora RPN \n\n\n");
		
		boolean done = false;

		while (!done) {
			System.out.println( "Digite um numero ou operador por linha ou S para sair ou R para remover o último valor: ");
			String input = in.nextLine();
			
			try{
				
				switch (input) {
				case "+":
					con.escolherOperacao( 1 );
					break;
					
				case "-":
					con.escolherOperacao( 2 );
					break;
				
				case "*":
					con.escolherOperacao( 3 );
					break;
					
				case "/":
					con.escolherOperacao( 4 );
					break;

				case "s":
				case "S":
					done = true;
					System.out.println( "\nBye Bye" );
					break;
					
				case "r":
				case "R":
					con.deletarDado();
					break;
					
				default:
					con.entrarDado(Double.parseDouble(input));
					break;
				}
				
				
			}catch( RPNWarningException e ){
				System.out.println( "\nAtenção:" + e.getMessage() );
			
			}catch( NumberFormatException e ){
				System.out.println( "\nValor informado não aceito" );
				
			}catch( Exception e ){
				System.out.println( "\n\nErro:" + e.getClass() + " - " + e.getMessage() + "\n\n" );
			}
			
			con.mostrarResultado();
			
		}

	}

	@Override
	public double multiplicar(double num1, double num2) {
		return num1 * num2;
	}

	@Override
	public double dividir(double num1, double num2) {
		return num1 / num2;
	}

	@Override
	public double somar(double num1, double num2) {
		return num1 + num2;
	}

	@Override
	public double subtrair(double num1, double num2) {
		return num1 - num2;
	};

}
